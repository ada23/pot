pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
with Interfaces.C.Strings;
with System;
with Interfaces.C.Extensions;
with stddef_h;
with utypes_uuint8_t_h;
with utypes_uuint16_t_h;
with utypes_uuint32_t_h;

package mosquitto_h is

   LIBMOSQUITTO_MAJOR : constant := 2;  --  /opt/homebrew/include/mosquitto.h:67
   LIBMOSQUITTO_MINOR : constant := 0;  --  /opt/homebrew/include/mosquitto.h:68
   LIBMOSQUITTO_REVISION : constant := 15;  --  /opt/homebrew/include/mosquitto.h:69
   --  unsupported macro: LIBMOSQUITTO_VERSION_NUMBER (LIBMOSQUITTO_MAJOR*1000000+LIBMOSQUITTO_MINOR*1000+LIBMOSQUITTO_REVISION)

   MOSQ_LOG_NONE : constant := 0;  --  /opt/homebrew/include/mosquitto.h:74
   MOSQ_LOG_INFO : constant := (2**0);  --  /opt/homebrew/include/mosquitto.h:75
   MOSQ_LOG_NOTICE : constant := (2**1);  --  /opt/homebrew/include/mosquitto.h:76
   MOSQ_LOG_WARNING : constant := (2**2);  --  /opt/homebrew/include/mosquitto.h:77
   MOSQ_LOG_ERR : constant := (2**3);  --  /opt/homebrew/include/mosquitto.h:78
   MOSQ_LOG_DEBUG : constant := (2**4);  --  /opt/homebrew/include/mosquitto.h:79
   MOSQ_LOG_SUBSCRIBE : constant := (2**5);  --  /opt/homebrew/include/mosquitto.h:80
   MOSQ_LOG_UNSUBSCRIBE : constant := (2**6);  --  /opt/homebrew/include/mosquitto.h:81
   MOSQ_LOG_WEBSOCKETS : constant := (2**7);  --  /opt/homebrew/include/mosquitto.h:82
   MOSQ_LOG_INTERNAL : constant := 16#80000000#;  --  /opt/homebrew/include/mosquitto.h:83
   MOSQ_LOG_ALL : constant := 16#FFFFFFFF#;  --  /opt/homebrew/include/mosquitto.h:84

   MOSQ_MQTT_ID_MAX_LENGTH : constant := 23;  --  /opt/homebrew/include/mosquitto.h:151

   MQTT_PROTOCOL_V31 : constant := 3;  --  /opt/homebrew/include/mosquitto.h:153
   MQTT_PROTOCOL_V311 : constant := 4;  --  /opt/homebrew/include/mosquitto.h:154
   MQTT_PROTOCOL_V5 : constant := 5;  --  /opt/homebrew/include/mosquitto.h:155

   subtype mosq_err_t is int;
   mosq_err_t_MOSQ_ERR_AUTH_CONTINUE : constant mosq_err_t := -4;
   mosq_err_t_MOSQ_ERR_NO_SUBSCRIBERS : constant mosq_err_t := -3;
   mosq_err_t_MOSQ_ERR_SUB_EXISTS : constant mosq_err_t := -2;
   mosq_err_t_MOSQ_ERR_CONN_PENDING : constant mosq_err_t := -1;
   mosq_err_t_MOSQ_ERR_SUCCESS : constant mosq_err_t := 0;
   mosq_err_t_MOSQ_ERR_NOMEM : constant mosq_err_t := 1;
   mosq_err_t_MOSQ_ERR_PROTOCOL : constant mosq_err_t := 2;
   mosq_err_t_MOSQ_ERR_INVAL : constant mosq_err_t := 3;
   mosq_err_t_MOSQ_ERR_NO_CONN : constant mosq_err_t := 4;
   mosq_err_t_MOSQ_ERR_CONN_REFUSED : constant mosq_err_t := 5;
   mosq_err_t_MOSQ_ERR_NOT_FOUND : constant mosq_err_t := 6;
   mosq_err_t_MOSQ_ERR_CONN_LOST : constant mosq_err_t := 7;
   mosq_err_t_MOSQ_ERR_TLS : constant mosq_err_t := 8;
   mosq_err_t_MOSQ_ERR_PAYLOAD_SIZE : constant mosq_err_t := 9;
   mosq_err_t_MOSQ_ERR_NOT_SUPPORTED : constant mosq_err_t := 10;
   mosq_err_t_MOSQ_ERR_AUTH : constant mosq_err_t := 11;
   mosq_err_t_MOSQ_ERR_ACL_DENIED : constant mosq_err_t := 12;
   mosq_err_t_MOSQ_ERR_UNKNOWN : constant mosq_err_t := 13;
   mosq_err_t_MOSQ_ERR_ERRNO : constant mosq_err_t := 14;
   mosq_err_t_MOSQ_ERR_EAI : constant mosq_err_t := 15;
   mosq_err_t_MOSQ_ERR_PROXY : constant mosq_err_t := 16;
   mosq_err_t_MOSQ_ERR_PLUGIN_DEFER : constant mosq_err_t := 17;
   mosq_err_t_MOSQ_ERR_MALFORMED_UTF8 : constant mosq_err_t := 18;
   mosq_err_t_MOSQ_ERR_KEEPALIVE : constant mosq_err_t := 19;
   mosq_err_t_MOSQ_ERR_LOOKUP : constant mosq_err_t := 20;
   mosq_err_t_MOSQ_ERR_MALFORMED_PACKET : constant mosq_err_t := 21;
   mosq_err_t_MOSQ_ERR_DUPLICATE_PROPERTY : constant mosq_err_t := 22;
   mosq_err_t_MOSQ_ERR_TLS_HANDSHAKE : constant mosq_err_t := 23;
   mosq_err_t_MOSQ_ERR_QOS_NOT_SUPPORTED : constant mosq_err_t := 24;
   mosq_err_t_MOSQ_ERR_OVERSIZE_PACKET : constant mosq_err_t := 25;
   mosq_err_t_MOSQ_ERR_OCSP : constant mosq_err_t := 26;
   mosq_err_t_MOSQ_ERR_TIMEOUT : constant mosq_err_t := 27;
   mosq_err_t_MOSQ_ERR_RETAIN_NOT_SUPPORTED : constant mosq_err_t := 28;
   mosq_err_t_MOSQ_ERR_TOPIC_ALIAS_INVALID : constant mosq_err_t := 29;
   mosq_err_t_MOSQ_ERR_ADMINISTRATIVE_ACTION : constant mosq_err_t := 30;
   mosq_err_t_MOSQ_ERR_ALREADY_EXISTS : constant mosq_err_t := 31;  -- /opt/homebrew/include/mosquitto.h:88

   subtype mosq_opt_t is unsigned;
   mosq_opt_t_MOSQ_OPT_PROTOCOL_VERSION : constant mosq_opt_t := 1;
   mosq_opt_t_MOSQ_OPT_SSL_CTX : constant mosq_opt_t := 2;
   mosq_opt_t_MOSQ_OPT_SSL_CTX_WITH_DEFAULTS : constant mosq_opt_t := 3;
   mosq_opt_t_MOSQ_OPT_RECEIVE_MAXIMUM : constant mosq_opt_t := 4;
   mosq_opt_t_MOSQ_OPT_SEND_MAXIMUM : constant mosq_opt_t := 5;
   mosq_opt_t_MOSQ_OPT_TLS_KEYFORM : constant mosq_opt_t := 6;
   mosq_opt_t_MOSQ_OPT_TLS_ENGINE : constant mosq_opt_t := 7;
   mosq_opt_t_MOSQ_OPT_TLS_ENGINE_KPASS_SHA1 : constant mosq_opt_t := 8;
   mosq_opt_t_MOSQ_OPT_TLS_OCSP_REQUIRED : constant mosq_opt_t := 9;
   mosq_opt_t_MOSQ_OPT_TLS_ALPN : constant mosq_opt_t := 10;
   mosq_opt_t_MOSQ_OPT_TCP_NODELAY : constant mosq_opt_t := 11;
   mosq_opt_t_MOSQ_OPT_BIND_ADDRESS : constant mosq_opt_t := 12;
   mosq_opt_t_MOSQ_OPT_TLS_USE_OS_CERTS : constant mosq_opt_t := 13;  -- /opt/homebrew/include/mosquitto.h:133

   type mosquitto_message is record
      mid : aliased int;  -- /opt/homebrew/include/mosquitto.h:176
      topic : Interfaces.C.Strings.chars_ptr;  -- /opt/homebrew/include/mosquitto.h:177
      payload : System.Address;  -- /opt/homebrew/include/mosquitto.h:178
      payloadlen : aliased int;  -- /opt/homebrew/include/mosquitto.h:179
      qos : aliased int;  -- /opt/homebrew/include/mosquitto.h:180
      retain : aliased Extensions.bool;  -- /opt/homebrew/include/mosquitto.h:181
   end record
   with Convention => C_Pass_By_Copy;  -- /opt/homebrew/include/mosquitto.h:175

   type mosquitto is null record;   -- incomplete struct

   type mqtt5_u_property is null record;   -- incomplete struct

   subtype mosquitto_property is mqtt5_u_property;  -- /opt/homebrew/include/mosquitto.h:185

   function mosquitto_lib_version
     (major : access int;
      minor : access int;
      revision : access int) return int  -- /opt/homebrew/include/mosquitto.h:248
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_lib_version";

   function mosquitto_lib_init return int  -- /opt/homebrew/include/mosquitto.h:264
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_lib_init";

   function mosquitto_lib_cleanup return int  -- /opt/homebrew/include/mosquitto.h:277
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_lib_cleanup";

   function mosquitto_new
     (id : Interfaces.C.Strings.chars_ptr;
      clean_session : Extensions.bool;
      obj : System.Address) return access mosquitto  -- /opt/homebrew/include/mosquitto.h:314
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_new";

   procedure mosquitto_destroy (mosq : access mosquitto)  -- /opt/homebrew/include/mosquitto.h:327
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_destroy";

   function mosquitto_reinitialise
     (mosq : access mosquitto;
      id : Interfaces.C.Strings.chars_ptr;
      clean_session : Extensions.bool;
      obj : System.Address) return int  -- /opt/homebrew/include/mosquitto.h:357
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_reinitialise";

   function mosquitto_will_set
     (mosq : access mosquitto;
      topic : Interfaces.C.Strings.chars_ptr;
      payloadlen : int;
      payload : System.Address;
      qos : int;
      retain : Extensions.bool) return int  -- /opt/homebrew/include/mosquitto.h:392
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_will_set";

   function mosquitto_will_set_v5
     (mosq : access mosquitto;
      topic : Interfaces.C.Strings.chars_ptr;
      payloadlen : int;
      payload : System.Address;
      qos : int;
      retain : Extensions.bool;
      properties : access mosquitto_property) return int  -- /opt/homebrew/include/mosquitto.h:435
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_will_set_v5";

   function mosquitto_will_clear (mosq : access mosquitto) return int  -- /opt/homebrew/include/mosquitto.h:450
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_will_clear";

   function mosquitto_username_pw_set
     (mosq : access mosquitto;
      username : Interfaces.C.Strings.chars_ptr;
      password : Interfaces.C.Strings.chars_ptr) return int  -- /opt/homebrew/include/mosquitto.h:479
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_username_pw_set";

   function mosquitto_connect
     (mosq : access mosquitto;
      host : Interfaces.C.Strings.chars_ptr;
      port : int;
      keepalive : int) return int  -- /opt/homebrew/include/mosquitto.h:519
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_connect";

   function mosquitto_connect_bind
     (mosq : access mosquitto;
      host : Interfaces.C.Strings.chars_ptr;
      port : int;
      keepalive : int;
      bind_address : Interfaces.C.Strings.chars_ptr) return int  -- /opt/homebrew/include/mosquitto.h:550
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_connect_bind";

   function mosquitto_connect_bind_v5
     (mosq : access mosquitto;
      host : Interfaces.C.Strings.chars_ptr;
      port : int;
      keepalive : int;
      bind_address : Interfaces.C.Strings.chars_ptr;
      properties : access constant mosquitto_property) return int  -- /opt/homebrew/include/mosquitto.h:601
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_connect_bind_v5";

   function mosquitto_connect_async
     (mosq : access mosquitto;
      host : Interfaces.C.Strings.chars_ptr;
      port : int;
      keepalive : int) return int  -- /opt/homebrew/include/mosquitto.h:632
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_connect_async";

   function mosquitto_connect_bind_async
     (mosq : access mosquitto;
      host : Interfaces.C.Strings.chars_ptr;
      port : int;
      keepalive : int;
      bind_address : Interfaces.C.Strings.chars_ptr) return int  -- /opt/homebrew/include/mosquitto.h:674
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_connect_bind_async";

   function mosquitto_connect_srv
     (mosq : access mosquitto;
      host : Interfaces.C.Strings.chars_ptr;
      keepalive : int;
      bind_address : Interfaces.C.Strings.chars_ptr) return int  -- /opt/homebrew/include/mosquitto.h:713
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_connect_srv";

   function mosquitto_reconnect (mosq : access mosquitto) return int  -- /opt/homebrew/include/mosquitto.h:740
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_reconnect";

   function mosquitto_reconnect_async (mosq : access mosquitto) return int  -- /opt/homebrew/include/mosquitto.h:767
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_reconnect_async";

   function mosquitto_disconnect (mosq : access mosquitto) return int  -- /opt/homebrew/include/mosquitto.h:786
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_disconnect";

   function mosquitto_disconnect_v5
     (mosq : access mosquitto;
      reason_code : int;
      properties : access constant mosquitto_property) return int  -- /opt/homebrew/include/mosquitto.h:817
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_disconnect_v5";

   function mosquitto_publish
     (mosq : access mosquitto;
      mid : access int;
      topic : Interfaces.C.Strings.chars_ptr;
      payloadlen : int;
      payload : System.Address;
      qos : int;
      retain : Extensions.bool) return int  -- /opt/homebrew/include/mosquitto.h:869
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_publish";

   function mosquitto_publish_v5
     (mosq : access mosquitto;
      mid : access int;
      topic : Interfaces.C.Strings.chars_ptr;
      payloadlen : int;
      payload : System.Address;
      qos : int;
      retain : Extensions.bool;
      properties : access constant mosquitto_property) return int  -- /opt/homebrew/include/mosquitto.h:924
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_publish_v5";

   function mosquitto_subscribe
     (mosq : access mosquitto;
      mid : access int;
      sub : Interfaces.C.Strings.chars_ptr;
      qos : int) return int  -- /opt/homebrew/include/mosquitto.h:962
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_subscribe";

   function mosquitto_subscribe_v5
     (mosq : access mosquitto;
      mid : access int;
      sub : Interfaces.C.Strings.chars_ptr;
      qos : int;
      options : int;
      properties : access constant mosquitto_property) return int  -- /opt/homebrew/include/mosquitto.h:1004
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_subscribe_v5";

   function mosquitto_subscribe_multiple
     (mosq : access mosquitto;
      mid : access int;
      sub_count : int;
      sub : System.Address;
      qos : int;
      options : int;
      properties : access constant mosquitto_property) return int  -- /opt/homebrew/include/mosquitto.h:1039
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_subscribe_multiple";

   function mosquitto_unsubscribe
     (mosq : access mosquitto;
      mid : access int;
      sub : Interfaces.C.Strings.chars_ptr) return int  -- /opt/homebrew/include/mosquitto.h:1063
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_unsubscribe";

   function mosquitto_unsubscribe_v5
     (mosq : access mosquitto;
      mid : access int;
      sub : Interfaces.C.Strings.chars_ptr;
      properties : access constant mosquitto_property) return int  -- /opt/homebrew/include/mosquitto.h:1107
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_unsubscribe_v5";

   function mosquitto_unsubscribe_multiple
     (mosq : access mosquitto;
      mid : access int;
      sub_count : int;
      sub : System.Address;
      properties : access constant mosquitto_property) return int  -- /opt/homebrew/include/mosquitto.h:1138
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_unsubscribe_multiple";

   function mosquitto_message_copy (dst : access mosquitto_message; src : access constant mosquitto_message) return int  -- /opt/homebrew/include/mosquitto.h:1164
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_message_copy";

   procedure mosquitto_message_free (message : System.Address)  -- /opt/homebrew/include/mosquitto.h:1177
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_message_free";

   procedure mosquitto_message_free_contents (message : access mosquitto_message)  -- /opt/homebrew/include/mosquitto.h:1190
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_message_free_contents";

   function mosquitto_loop_forever
     (mosq : access mosquitto;
      timeout : int;
      max_packets : int) return int  -- /opt/homebrew/include/mosquitto.h:1242
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_loop_forever";

   function mosquitto_loop_start (mosq : access mosquitto) return int  -- /opt/homebrew/include/mosquitto.h:1262
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_loop_start";

   function mosquitto_loop_stop (mosq : access mosquitto; force : Extensions.bool) return int  -- /opt/homebrew/include/mosquitto.h:1286
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_loop_stop";

   function mosquitto_loop
     (mosq : access mosquitto;
      timeout : int;
      max_packets : int) return int  -- /opt/homebrew/include/mosquitto.h:1333
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_loop";

   function mosquitto_loop_read (mosq : access mosquitto; max_packets : int) return int  -- /opt/homebrew/include/mosquitto.h:1368
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_loop_read";

   function mosquitto_loop_write (mosq : access mosquitto; max_packets : int) return int  -- /opt/homebrew/include/mosquitto.h:1398
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_loop_write";

   function mosquitto_loop_misc (mosq : access mosquitto) return int  -- /opt/homebrew/include/mosquitto.h:1422
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_loop_misc";

   function mosquitto_socket (mosq : access mosquitto) return int  -- /opt/homebrew/include/mosquitto.h:1442
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_socket";

   function mosquitto_want_write (mosq : access mosquitto) return Extensions.bool  -- /opt/homebrew/include/mosquitto.h:1455
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_want_write";

   function mosquitto_threaded_set (mosq : access mosquitto; threaded : Extensions.bool) return int  -- /opt/homebrew/include/mosquitto.h:1472
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_threaded_set";

   function mosquitto_opts_set
     (mosq : access mosquitto;
      option : mosq_opt_t;
      value : System.Address) return int  -- /opt/homebrew/include/mosquitto.h:1518
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_opts_set";

   function mosquitto_int_option
     (mosq : access mosquitto;
      option : mosq_opt_t;
      value : int) return int  -- /opt/homebrew/include/mosquitto.h:1576
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_int_option";

   function mosquitto_string_option
     (mosq : access mosquitto;
      option : mosq_opt_t;
      value : Interfaces.C.Strings.chars_ptr) return int  -- /opt/homebrew/include/mosquitto.h:1617
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_string_option";

   function mosquitto_void_option
     (mosq : access mosquitto;
      option : mosq_opt_t;
      value : System.Address) return int  -- /opt/homebrew/include/mosquitto.h:1640
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_void_option";

   function mosquitto_reconnect_delay_set
     (mosq : access mosquitto;
      reconnect_delay : unsigned;
      reconnect_delay_max : unsigned;
      reconnect_exponential_backoff : Extensions.bool) return int  -- /opt/homebrew/include/mosquitto.h:1677
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_reconnect_delay_set";

   function mosquitto_max_inflight_messages_set (mosq : access mosquitto; max_inflight_messages : unsigned) return int  -- /opt/homebrew/include/mosquitto.h:1705
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_max_inflight_messages_set";

   procedure mosquitto_message_retry_set (mosq : access mosquitto; message_retry : unsigned)  -- /opt/homebrew/include/mosquitto.h:1712
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_message_retry_set";

   procedure mosquitto_user_data_set (mosq : access mosquitto; obj : System.Address)  -- /opt/homebrew/include/mosquitto.h:1728
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_user_data_set";

   function mosquitto_userdata (mosq : access mosquitto) return System.Address  -- /opt/homebrew/include/mosquitto.h:1740
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_userdata";

   function mosquitto_tls_set
     (mosq : access mosquitto;
      cafile : Interfaces.C.Strings.chars_ptr;
      capath : Interfaces.C.Strings.chars_ptr;
      certfile : Interfaces.C.Strings.chars_ptr;
      keyfile : Interfaces.C.Strings.chars_ptr;
      pw_callback : access function
        (arg1 : Interfaces.C.Strings.chars_ptr;
         arg2 : int;
         arg3 : int;
         arg4 : System.Address) return int) return int  -- /opt/homebrew/include/mosquitto.h:1796
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_tls_set";

   function mosquitto_tls_insecure_set (mosq : access mosquitto; value : Extensions.bool) return int  -- /opt/homebrew/include/mosquitto.h:1826
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_tls_insecure_set";

   function mosquitto_tls_opts_set
     (mosq : access mosquitto;
      cert_reqs : int;
      tls_version : Interfaces.C.Strings.chars_ptr;
      ciphers : Interfaces.C.Strings.chars_ptr) return int  -- /opt/homebrew/include/mosquitto.h:1860
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_tls_opts_set";

   function mosquitto_tls_psk_set
     (mosq : access mosquitto;
      psk : Interfaces.C.Strings.chars_ptr;
      identity : Interfaces.C.Strings.chars_ptr;
      ciphers : Interfaces.C.Strings.chars_ptr) return int  -- /opt/homebrew/include/mosquitto.h:1887
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_tls_psk_set";

   function mosquitto_ssl_get (mosq : access mosquitto) return System.Address  -- /opt/homebrew/include/mosquitto.h:1904
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_ssl_get";

   procedure mosquitto_connect_callback_set (mosq : access mosquitto; on_connect : access procedure
        (arg1 : access mosquitto;
         arg2 : System.Address;
         arg3 : int))  -- /opt/homebrew/include/mosquitto.h:1931
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_connect_callback_set";

   procedure mosquitto_connect_with_flags_callback_set (mosq : access mosquitto; on_connect : access procedure
        (arg1 : access mosquitto;
         arg2 : System.Address;
         arg3 : int;
         arg4 : int))  -- /opt/homebrew/include/mosquitto.h:1953
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_connect_with_flags_callback_set";

   procedure mosquitto_connect_v5_callback_set (mosq : access mosquitto; on_connect : access procedure
        (arg1 : access mosquitto;
         arg2 : System.Address;
         arg3 : int;
         arg4 : int;
         arg5 : access constant mosquitto_property))  -- /opt/homebrew/include/mosquitto.h:1981
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_connect_v5_callback_set";

   procedure mosquitto_disconnect_callback_set (mosq : access mosquitto; on_disconnect : access procedure
        (arg1 : access mosquitto;
         arg2 : System.Address;
         arg3 : int))  -- /opt/homebrew/include/mosquitto.h:2001
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_disconnect_callback_set";

   procedure mosquitto_disconnect_v5_callback_set (mosq : access mosquitto; on_disconnect : access procedure
        (arg1 : access mosquitto;
         arg2 : System.Address;
         arg3 : int;
         arg4 : access constant mosquitto_property))  -- /opt/homebrew/include/mosquitto.h:2026
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_disconnect_v5_callback_set";

   procedure mosquitto_publish_callback_set (mosq : access mosquitto; on_publish : access procedure
        (arg1 : access mosquitto;
         arg2 : System.Address;
         arg3 : int))  -- /opt/homebrew/include/mosquitto.h:2052
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_publish_callback_set";

   procedure mosquitto_publish_v5_callback_set (mosq : access mosquitto; on_publish : access procedure
        (arg1 : access mosquitto;
         arg2 : System.Address;
         arg3 : int;
         arg4 : int;
         arg5 : access constant mosquitto_property))  -- /opt/homebrew/include/mosquitto.h:2087
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_publish_v5_callback_set";

   procedure mosquitto_message_callback_set (mosq : access mosquitto; on_message : access procedure
        (arg1 : access mosquitto;
         arg2 : System.Address;
         arg3 : access constant mosquitto_message))  -- /opt/homebrew/include/mosquitto.h:2110
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_message_callback_set";

   procedure mosquitto_message_v5_callback_set (mosq : access mosquitto; on_message : access procedure
        (arg1 : access mosquitto;
         arg2 : System.Address;
         arg3 : access constant mosquitto_message;
         arg4 : access constant mosquitto_property))  -- /opt/homebrew/include/mosquitto.h:2138
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_message_v5_callback_set";

   procedure mosquitto_subscribe_callback_set (mosq : access mosquitto; on_subscribe : access procedure
        (arg1 : access mosquitto;
         arg2 : System.Address;
         arg3 : int;
         arg4 : int;
         arg5 : access int))  -- /opt/homebrew/include/mosquitto.h:2159
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_subscribe_callback_set";

   procedure mosquitto_subscribe_v5_callback_set (mosq : access mosquitto; on_subscribe : access procedure
        (arg1 : access mosquitto;
         arg2 : System.Address;
         arg3 : int;
         arg4 : int;
         arg5 : access int;
         arg6 : access constant mosquitto_property))  -- /opt/homebrew/include/mosquitto.h:2185
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_subscribe_v5_callback_set";

   procedure mosquitto_unsubscribe_callback_set (mosq : access mosquitto; on_unsubscribe : access procedure
        (arg1 : access mosquitto;
         arg2 : System.Address;
         arg3 : int))  -- /opt/homebrew/include/mosquitto.h:2203
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_unsubscribe_callback_set";

   procedure mosquitto_unsubscribe_v5_callback_set (mosq : access mosquitto; on_unsubscribe : access procedure
        (arg1 : access mosquitto;
         arg2 : System.Address;
         arg3 : int;
         arg4 : access constant mosquitto_property))  -- /opt/homebrew/include/mosquitto.h:2226
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_unsubscribe_v5_callback_set";

   procedure mosquitto_log_callback_set (mosq : access mosquitto; on_log : access procedure
        (arg1 : access mosquitto;
         arg2 : System.Address;
         arg3 : int;
         arg4 : Interfaces.C.Strings.chars_ptr))  -- /opt/homebrew/include/mosquitto.h:2249
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_log_callback_set";

   function mosquitto_socks5_set
     (mosq : access mosquitto;
      host : Interfaces.C.Strings.chars_ptr;
      port : int;
      username : Interfaces.C.Strings.chars_ptr;
      password : Interfaces.C.Strings.chars_ptr) return int  -- /opt/homebrew/include/mosquitto.h:2274
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_socks5_set";

   function mosquitto_strerror (mosq_errno : int) return Interfaces.C.Strings.chars_ptr  -- /opt/homebrew/include/mosquitto.h:2295
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_strerror";

   function mosquitto_connack_string (connack_code : int) return Interfaces.C.Strings.chars_ptr  -- /opt/homebrew/include/mosquitto.h:2308
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_connack_string";

   function mosquitto_reason_string (reason_code : int) return Interfaces.C.Strings.chars_ptr  -- /opt/homebrew/include/mosquitto.h:2321
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_reason_string";

   function mosquitto_string_to_command (str : Interfaces.C.Strings.chars_ptr; cmd : access int) return int  -- /opt/homebrew/include/mosquitto.h:2342
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_string_to_command";

   function mosquitto_sub_topic_tokenise
     (subtopic : Interfaces.C.Strings.chars_ptr;
      topics : System.Address;
      count : access int) return int  -- /opt/homebrew/include/mosquitto.h:2398
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_sub_topic_tokenise";

   function mosquitto_sub_topic_tokens_free (topics : System.Address; count : int) return int  -- /opt/homebrew/include/mosquitto.h:2416
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_sub_topic_tokens_free";

   function mosquitto_topic_matches_sub
     (sub : Interfaces.C.Strings.chars_ptr;
      topic : Interfaces.C.Strings.chars_ptr;
      result : access Extensions.bool) return int  -- /opt/homebrew/include/mosquitto.h:2439
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_topic_matches_sub";

   function mosquitto_topic_matches_sub2
     (sub : Interfaces.C.Strings.chars_ptr;
      sublen : stddef_h.size_t;
      topic : Interfaces.C.Strings.chars_ptr;
      topiclen : stddef_h.size_t;
      result : access Extensions.bool) return int  -- /opt/homebrew/include/mosquitto.h:2465
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_topic_matches_sub2";

   function mosquitto_pub_topic_check (topic : Interfaces.C.Strings.chars_ptr) return int  -- /opt/homebrew/include/mosquitto.h:2490
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_pub_topic_check";

   function mosquitto_pub_topic_check2 (topic : Interfaces.C.Strings.chars_ptr; topiclen : stddef_h.size_t) return int  -- /opt/homebrew/include/mosquitto.h:2516
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_pub_topic_check2";

   function mosquitto_sub_topic_check (topic : Interfaces.C.Strings.chars_ptr) return int  -- /opt/homebrew/include/mosquitto.h:2544
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_sub_topic_check";

   function mosquitto_sub_topic_check2 (topic : Interfaces.C.Strings.chars_ptr; topiclen : stddef_h.size_t) return int  -- /opt/homebrew/include/mosquitto.h:2573
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_sub_topic_check2";

   function mosquitto_validate_utf8 (str : Interfaces.C.Strings.chars_ptr; len : int) return int  -- /opt/homebrew/include/mosquitto.h:2591
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_validate_utf8";

   type libmosquitto_will is record
      topic : Interfaces.C.Strings.chars_ptr;  -- /opt/homebrew/include/mosquitto.h:2602
      payload : System.Address;  -- /opt/homebrew/include/mosquitto.h:2603
      payloadlen : aliased int;  -- /opt/homebrew/include/mosquitto.h:2604
      qos : aliased int;  -- /opt/homebrew/include/mosquitto.h:2605
      retain : aliased Extensions.bool;  -- /opt/homebrew/include/mosquitto.h:2606
   end record
   with Convention => C_Pass_By_Copy;  -- /opt/homebrew/include/mosquitto.h:2601

   type libmosquitto_auth is record
      username : Interfaces.C.Strings.chars_ptr;  -- /opt/homebrew/include/mosquitto.h:2610
      password : Interfaces.C.Strings.chars_ptr;  -- /opt/homebrew/include/mosquitto.h:2611
   end record
   with Convention => C_Pass_By_Copy;  -- /opt/homebrew/include/mosquitto.h:2609

   type libmosquitto_tls is record
      cafile : Interfaces.C.Strings.chars_ptr;  -- /opt/homebrew/include/mosquitto.h:2615
      capath : Interfaces.C.Strings.chars_ptr;  -- /opt/homebrew/include/mosquitto.h:2616
      certfile : Interfaces.C.Strings.chars_ptr;  -- /opt/homebrew/include/mosquitto.h:2617
      keyfile : Interfaces.C.Strings.chars_ptr;  -- /opt/homebrew/include/mosquitto.h:2618
      ciphers : Interfaces.C.Strings.chars_ptr;  -- /opt/homebrew/include/mosquitto.h:2619
      tls_version : Interfaces.C.Strings.chars_ptr;  -- /opt/homebrew/include/mosquitto.h:2620
      pw_callback : access function
           (arg1 : Interfaces.C.Strings.chars_ptr;
            arg2 : int;
            arg3 : int;
            arg4 : System.Address) return int;  -- /opt/homebrew/include/mosquitto.h:2621
      cert_reqs : aliased int;  -- /opt/homebrew/include/mosquitto.h:2622
   end record
   with Convention => C_Pass_By_Copy;  -- /opt/homebrew/include/mosquitto.h:2614

   function mosquitto_subscribe_simple
     (messages : System.Address;
      msg_count : int;
      want_retained : Extensions.bool;
      topic : Interfaces.C.Strings.chars_ptr;
      qos : int;
      host : Interfaces.C.Strings.chars_ptr;
      port : int;
      client_id : Interfaces.C.Strings.chars_ptr;
      keepalive : int;
      clean_session : Extensions.bool;
      username : Interfaces.C.Strings.chars_ptr;
      password : Interfaces.C.Strings.chars_ptr;
      will : access constant libmosquitto_will;
      tls : access constant libmosquitto_tls) return int  -- /opt/homebrew/include/mosquitto.h:2662
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_subscribe_simple";

   function mosquitto_subscribe_callback
     (callback : access function
        (arg1 : access mosquitto;
         arg2 : System.Address;
         arg3 : access constant mosquitto_message) return int;
      userdata : System.Address;
      topic : Interfaces.C.Strings.chars_ptr;
      qos : int;
      host : Interfaces.C.Strings.chars_ptr;
      port : int;
      client_id : Interfaces.C.Strings.chars_ptr;
      keepalive : int;
      clean_session : Extensions.bool;
      username : Interfaces.C.Strings.chars_ptr;
      password : Interfaces.C.Strings.chars_ptr;
      will : access constant libmosquitto_will;
      tls : access constant libmosquitto_tls) return int  -- /opt/homebrew/include/mosquitto.h:2715
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_subscribe_callback";

   function mosquitto_property_add_byte
     (proplist : System.Address;
      identifier : int;
      value : utypes_uuint8_t_h.uint8_t) return int  -- /opt/homebrew/include/mosquitto.h:2761
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_property_add_byte";

   function mosquitto_property_add_int16
     (proplist : System.Address;
      identifier : int;
      value : utypes_uuint16_t_h.uint16_t) return int  -- /opt/homebrew/include/mosquitto.h:2785
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_property_add_int16";

   function mosquitto_property_add_int32
     (proplist : System.Address;
      identifier : int;
      value : utypes_uuint32_t_h.uint32_t) return int  -- /opt/homebrew/include/mosquitto.h:2809
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_property_add_int32";

   function mosquitto_property_add_varint
     (proplist : System.Address;
      identifier : int;
      value : utypes_uuint32_t_h.uint32_t) return int  -- /opt/homebrew/include/mosquitto.h:2833
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_property_add_varint";

   function mosquitto_property_add_binary
     (proplist : System.Address;
      identifier : int;
      value : System.Address;
      len : utypes_uuint16_t_h.uint16_t) return int  -- /opt/homebrew/include/mosquitto.h:2858
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_property_add_binary";

   function mosquitto_property_add_string
     (proplist : System.Address;
      identifier : int;
      value : Interfaces.C.Strings.chars_ptr) return int  -- /opt/homebrew/include/mosquitto.h:2883
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_property_add_string";

   function mosquitto_property_add_string_pair
     (proplist : System.Address;
      identifier : int;
      name : Interfaces.C.Strings.chars_ptr;
      value : Interfaces.C.Strings.chars_ptr) return int  -- /opt/homebrew/include/mosquitto.h:2909
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_property_add_string_pair";

   function mosquitto_property_identifier (property : access constant mosquitto_property) return int  -- /opt/homebrew/include/mosquitto.h:2924
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_property_identifier";

   function mosquitto_property_next (proplist : access constant mosquitto_property) return access constant mosquitto_property  -- /opt/homebrew/include/mosquitto.h:2948
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_property_next";

   function mosquitto_property_read_byte
     (proplist : access constant mosquitto_property;
      identifier : int;
      value : access unsigned_char;
      skip_first : Extensions.bool) return access constant mosquitto_property  -- /opt/homebrew/include/mosquitto.h:2990
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_property_read_byte";

   function mosquitto_property_read_int16
     (proplist : access constant mosquitto_property;
      identifier : int;
      value : access unsigned_short;
      skip_first : Extensions.bool) return access constant mosquitto_property  -- /opt/homebrew/include/mosquitto.h:3015
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_property_read_int16";

   function mosquitto_property_read_int32
     (proplist : access constant mosquitto_property;
      identifier : int;
      value : access unsigned;
      skip_first : Extensions.bool) return access constant mosquitto_property  -- /opt/homebrew/include/mosquitto.h:3040
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_property_read_int32";

   function mosquitto_property_read_varint
     (proplist : access constant mosquitto_property;
      identifier : int;
      value : access unsigned;
      skip_first : Extensions.bool) return access constant mosquitto_property  -- /opt/homebrew/include/mosquitto.h:3065
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_property_read_varint";

   function mosquitto_property_read_binary
     (proplist : access constant mosquitto_property;
      identifier : int;
      value : System.Address;
      len : access unsigned_short;
      skip_first : Extensions.bool) return access constant mosquitto_property  -- /opt/homebrew/include/mosquitto.h:3092
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_property_read_binary";

   function mosquitto_property_read_string
     (proplist : access constant mosquitto_property;
      identifier : int;
      value : System.Address;
      skip_first : Extensions.bool) return access constant mosquitto_property  -- /opt/homebrew/include/mosquitto.h:3121
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_property_read_string";

   function mosquitto_property_read_string_pair
     (proplist : access constant mosquitto_property;
      identifier : int;
      name : System.Address;
      value : System.Address;
      skip_first : Extensions.bool) return access constant mosquitto_property  -- /opt/homebrew/include/mosquitto.h:3151
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_property_read_string_pair";

   procedure mosquitto_property_free_all (properties : System.Address)  -- /opt/homebrew/include/mosquitto.h:3171
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_property_free_all";

   function mosquitto_property_copy_all (dest : System.Address; src : access constant mosquitto_property) return int  -- /opt/homebrew/include/mosquitto.h:3185
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_property_copy_all";

   function mosquitto_property_check_command (command : int; identifier : int) return int  -- /opt/homebrew/include/mosquitto.h:3200
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_property_check_command";

   function mosquitto_property_check_all (command : int; properties : access constant mosquitto_property) return int  -- /opt/homebrew/include/mosquitto.h:3223
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_property_check_all";

   function mosquitto_property_identifier_to_string (identifier : int) return Interfaces.C.Strings.chars_ptr  -- /opt/homebrew/include/mosquitto.h:3239
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_property_identifier_to_string";

   function mosquitto_string_to_property_info
     (propname : Interfaces.C.Strings.chars_ptr;
      identifier : access int;
      c_type : access int) return int  -- /opt/homebrew/include/mosquitto.h:3264
   with Import => True, 
        Convention => C, 
        External_Name => "mosquitto_string_to_property_info";

end mosquitto_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");

with Ada.Text_Io; use Ada.Text_Io;

package body handlers is
  procedure strPayload( msg : access constant mosquitto_h.mosquitto_message ) is
      datas : String(1..Integer(msg.payloadlen));
      for datas'Address use msg.payload;
   begin
      Put_Line(datas);
   end strPayload ;
end handlers;
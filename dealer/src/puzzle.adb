with numexpr;
package body puzzle is
    function Verify(p : PuzzleType; ans : String) return Boolean is
    begin
        if p.answer = ans
        then
            return true;
        end if;
        return false;
    end Verify;

    protected body
    CurrentPuzzle is
        procedure Set( p : PuzzleType) is
        begin
            active := p;
            setTime := Ada.Calendar.Clock;
        end Set;
        function Get return PuzzleType is
        begin
            return active;
        end Get;
    end CurrentPuzzle ;

    task type NumExprGeneratorType is
        entry Start( ng : NumericExpressionGenerator);
    end NumExprGeneratorType;

    task body NumExprGeneratorType is
        timer : float ;
    begin
        accept Start( ng : NumericExpressionGenerator )
        do
            timer := float(ng.cadence);
        end Start;
        while True
        loop
            delay duration(timer * 60.0);
        end loop;
    end NumExprGeneratorType ;

    numexprgenerator : NumExprGeneratorType;

    function Create( c : Short_Short_Integer) return NumericExpressionGenerator is
        result : NumericExpressionGenerator;
    begin
        return result;
    end Create ;

    procedure Start( g : NumericExpressionGenerator) is
    begin
        null;
    end Start;

end puzzle ;

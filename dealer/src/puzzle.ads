with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;
with Ada.Calendar;

package puzzle is

    type PuzzleType is
    record
        question : Unbounded_String;
        answer : Unbounded_String;
    end record;

    function Verify(p : PuzzleType; ans : String) return Boolean;

    protected
    CurrentPuzzle is
        procedure Set( p : PuzzleType);
        function Get return PuzzleType;
    private
        setTime : Ada.Calendar.Time;
        active : PuzzleType;
    end CurrentPuzzle ;

    type Generator is abstract tagged 
    record
        cadence : Short_Short_Integer;
    end record;

    function Create( c : Short_Short_Integer ) return Generator is abstract;
    procedure Start(g : Generator) is abstract;

    type NumericExpressionGenerator is new Generator with null record ;
    function Create( c : Short_Short_Integer) return NumericExpressionGenerator;
    procedure Start( g : NumericExpressionGenerator);
    
end puzzle ;
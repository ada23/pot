with System;
with Ada.Text_Io; use Ada.Text_Io;
with Ada.Integer_Text_Io; use Ada.Integer_Text_Io;
with Interfaces.C.Strings; use Interfaces.C.Strings;
package body mqtt.subscriber is

    procedure cb_subscribe (arg1 : access mosquitto_h.mosquitto;
                          obj : System.Address;
                          mid : int;
                          qos_count : int;
                          granted_qos : access int
         ) ;
    Pragma Convention(C,cb_Subscribe);

    procedure cb_subscribe (arg1 : access mosquitto_h.mosquitto;
                          obj : System.Address;
                          mid : int;
                          qos_count : int;
                          granted_qos : access int)   
                        is
    begin
        Put_Line("On_subscribe");
    end cb_subscribe; 

    handler : MessageHandler ;
    procedure cb_message(arg1 : access mosquitto_h.mosquitto;
                         obj : System.Address;
                         msg : access constant mosquitto_h.mosquitto_message) ;
    Pragma Convention(C,cb_message);
    procedure cb_message(arg1 : access mosquitto_h.mosquitto;
                         obj : System.Address;
                         msg : access constant mosquitto_h.mosquitto_message) is
    begin
        if verbose
        then
            Put("mid: "); Put(Integer(msg.mid));
            Put(" topic: "); Put(Value(msg.topic));
            Put(" payloadlen: "); Put(Integer(msg.payloadlen));
            Put(" qos: "); Put(Integer(msg.qos));
            Put(" retain: "); Put(boolean'Image(boolean(msg.retain)));
            New_Line;
        end if ;

        handler.all(msg) ;
    end cb_message ;

    procedure Connect(h : handle ;
                      msghandler : not null MessageHandler ; 
                      topic : String ;
                      server : String := test_server ;
                      port : Integer := test_server_port ;
                      keepalive : Integer := 60 ) is
        mid : aliased Int := 0;
        status : Int ;
    begin
        if verbose
        then 
            mosquitto_h.mosquitto_subscribe_callback_set( h , cb_subscribe'access) ;
        end if ;
        handler := msghandler ;
        mosquitto_h.mosquitto_message_callback_set( h , cb_message'access) ;

        status := mosquitto_h.mosquitto_connect( h ,
                                        Interfaces.C.Strings.New_String(server) ,
                                        Int(port),
                                        Int(keepalive));
        if Status /= mosquitto_h.mosq_err_t_MOSQ_ERR_SUCCESS
        then
            raise CONNECT_FAILURE with ErrorMessage(Status) ;
        end if ;

        status := mosquitto_h.mosquitto_subscribe( h , mid'Access , Interfaces.C.Strings.New_String(topic), 1 );
        if status /= mosquitto_h.mosq_err_t_MOSQ_ERR_SUCCESS
        then
            raise SUBSCRIBE_ERROR with ErrorMessage(status) ;
        end if ;
    end Connect;

    procedure Start( h : handle ) is
        Status : Int ;
    begin
        Status := mosquitto_h.mosquitto_loop_forever(h, -1, 1);
        if Status /= 0
        then
            Put_Line( ErrorMessage(Status) );
        end if;
    end Start ;

end mqtt.subscriber ;
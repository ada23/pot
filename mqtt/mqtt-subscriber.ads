
package mqtt.subscriber is
    SUBSCRIBE_ERROR : exception;
    type MessageHandler is 
        access procedure ( msg : access constant mosquitto_h.mosquitto_message);

    procedure Connect(h : handle ;
                      msghandler : not null MessageHandler ;
                      topic : String ;
                      server : String := test_server ;
                      port : Integer := test_server_port ;
                      keepalive : Integer := 60 );
    procedure Start( h : handle );
end mqtt.subscriber;

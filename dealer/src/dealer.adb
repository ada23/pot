with Ada.Text_Io; use Ada.Text_Io;
with Ada.Integer_Text_Io; use Ada.Integer_Text_Io;
with Interfaces.C; use Interfaces.C;
with Ada.Calendar; use Ada.Calendar;
with Ada.Streams;
with Ada.Command_Line; use Ada.Command_Line;

with GNAT.Calendar.Time_Io ;
with mosquitto_h; use mosquitto_h;
with mqtt;
with mqtt.publisher;
with mqtt.subscriber;
with handlers;
with Puzzle;

with numexpr ;

procedure dealer is

 
   procedure dealer_publish is
      h : mqtt.Handle ;
      status : int ;
      major, minor, rev : aliased int ;
      expr : numexpr.ExpressionPtrType;
   begin
      h := mqtt.Create ;
      status := mosquitto_h.mosquitto_lib_version( major'Access , minor'Access, rev'access);
      Put("Version status "); Put(Integer(status)); New_Line;
      Put(Integer(major)) ; 
      Put(Integer(minor)) ; 
      Put(Integer(rev)); New_Line;

      Put_Line(mqtt.ErrorMessage(mosquitto_h.mosq_err_t_MOSQ_ERR_MALFORMED_UTF8));
      mqtt.Publisher.Connect(h) ;
      mqtt.Publisher.Start(h);
      
      for n in 1..10
      loop
         Put("Publishing "); Put(n); 
         expr := numexpr.Generate(2);
         -- mqtt.publisher.Publish(h,"potm/time" , gnat.Calendar.Time_io.Image(Clock,"%H:%M:%S"));
         mqtt.publisher.Publish(h,"potm/time" , numexpr.Image(expr)); 
         --gnat.Calendar.Time_io.Image(Clock,"%H:%M:%S"));
         Put_Line(" Done");
         delay 10.0;
      end loop ;
   end dealer_publish; 

   task publisher;
   task body publisher is
   begin
      Put_Line("Publisher");
      dealer_publish;
   end publisher;

   procedure dealer_subscribe is   
      subh : mqtt.handle;
   begin
      subh := mqtt.Create ;
      mqtt.subscriber.Connect(subh,handlers.strPayload'Access,"potm/time/#");
      mqtt.subscriber.Start(subh) ;
   end dealer_subscribe;

begin
   Put_Line("Subscriber");
   dealer_subscribe;  
   mqtt.Cleanup ;
end dealer ;
package mqtt.Publisher is

    procedure Connect(h : handle ;
                      server : String := test_server ;
                      port : Integer := test_server_port ;
                      will_topic : access String := null;
                      will_msg : access String := null;
                      keepalive : Integer := 60 );
    procedure Start( h : handle );

    procedure Publish( h : handle ;
                       topic : String ;
                       payload : String ;
                       qos : Integer := 2 ;
                       retain : boolean := false ) ;

end mqtt.Publisher;
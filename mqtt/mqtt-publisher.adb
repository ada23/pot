with System;
with Ada.Text_Io; use Ada.Text_Io;
with Interfaces.C.Strings; use Interfaces.C.Strings;
with Interfaces.C.Extensions; use Interfaces.C.Extensions;

package body mqtt.Publisher is


    procedure cb_publish (arg1 : access mosquitto_h.mosquitto;
                          obj : System.Address;
                          mid : int) ;
    Pragma Convention(C,cb_Publish);

    procedure cb_publish (arg1 : access mosquitto_h.mosquitto;
                          obj : System.Address;
                          mid : int) 
                        is
    begin
        Put_Line("On_Publish");
    end cb_publish ;

    procedure Connect(h : handle ;
                      server : String := test_server ;
                      port : Integer := test_server_port ;
                      will_topic : access String := null;
                      will_msg : access String := null;
                      keepalive : Integer := 60 ) is
        status : Int ;
    begin
        if verbose
        then
            mosquitto_h.mosquitto_publish_callback_set( h , cb_publish'access) ;
            mosquitto_h.mosquitto_disconnect_callback_set( h , cb_disconnect'access) ;
        end if;
        if will_topic /= null and then
           will_msg /= null
        then
            declare
                will_msg_payload : String := will_msg.all & ASCII.NUL;
            begin
                status := mosquitto_h.mosquitto_will_set( h 
                                                    , New_String(will_topic.all) 
                                                    , will_msg'length+1 
                                                    , will_msg_payload'Address 
                                                    , 0 
                                                    , true ) ;
                if status /= mosquitto_h.mosq_err_t_MOSQ_ERR_SUCCESS
                then
                    Put_Line(ErrorMessage(status));
                end if ;
            end ;
        end if ;

        status := mosquitto_h.mosquitto_connect( h ,
                                        Interfaces.C.Strings.New_String(server) ,
                                        Int(port),
                                        Int(keepalive));
        if Status /= mosquitto_h.mosq_err_t_MOSQ_ERR_SUCCESS
        then
            raise CONNECT_FAILURE with ErrorMessage(Status) ;
        end if ;
    end Connect;

    procedure Start( h : handle ) is
        status : int ;
    begin
        status := mosquitto_h.mosquitto_loop_start(h);
        if status /= mosquitto_h.mosq_err_t_MOSQ_ERR_SUCCESS
        then
            raise CONNECT_FAILURE with ErrorMessage(Status) ;
        end if ;
        delay 1.0;
    end Start ;

    procedure Publish( h : handle ;
                       topic : String ;
                       payload : String ;
                       qos : Integer := 2 ;
                       retain : boolean := false ) is
        ctopic : Interfaces.C.Strings.Chars_Ptr := Interfaces.C.Strings.New_String(topic);
        cretain : Interfaces.C.Extensions.bool := false ;
        status : Int ;
    begin
        if retain
        then
            cretain := true ;
        end if ;
        status := mosquitto_h.mosquitto_publish( h , mid => null , 
                            topic => ctopic , 
                            payloadlen => Int(payload'length) , 
                            payload => payload'Address , 
                            qos => Int(qos) , 
                            retain => cretain );
        if status /= mosquitto_h.mosq_err_t_MOSQ_ERR_SUCCESS
        then
            Put_Line(ErrorMessage(status));
        end if ;
    end Publish ;
end mqtt.Publisher;

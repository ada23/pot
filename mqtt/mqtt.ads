with Interfaces.C; use Interfaces.C;
with System;

with mosquitto_h ;

package mqtt is

   INITIALIZATION_FAILURE : exception ;
   HANDLE_CREATION_FAILURE : exception ;
   CONNECT_FAILURE : exception ;

    verbose : boolean := true ;

   procedure Cleanup;
   function ErrorMessage (code : int) return String ;

   type handle is private ;
   function Create ( id : access String := null ) return handle ;  
   procedure Destroy (h : handle) ;

   test_server : constant String := "test.mosquitto.org" ;
   -- These use cleartext messages
   test_server_port : constant Integer := 1883 ;       -- No Username, password
   test_server_port_auth : constant Integer := 1884 ;  -- Authenticated. username+password


private
   type Handle is access all mosquitto_h.mosquitto;
   procedure cb_connect (arg1 : access mosquitto_h.mosquitto;
                          obj : System.Address;
                          reason_code : int);
   Pragma Convention(C,cb_Connect); 

   procedure cb_disconnect (arg1 : access mosquitto_h.mosquitto;
                             obj : System.Address;
                             reason_code : int) ;
   Pragma Convention(C,cb_Disconnect);

end mqtt ;

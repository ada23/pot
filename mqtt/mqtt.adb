with System;
with Interfaces.C; use Interfaces.C;
with Interfaces.C.Strings; use Interfaces.C.Strings;
with Interfaces.C.Extensions; use Interfaces.C.Extensions;
with Ada.Text_Io; use Ada.Text_Io;

with mosquitto_h ;
package body mqtt is
    procedure Cleanup is
        status : int ;
    begin
        status := mosquitto_h.mosquitto_lib_cleanup ;
    end Cleanup;

    function ErrorMessage (code : int) return String is
    begin
        return Value(mosquitto_h.mosquitto_strerror(code)) ;
    end ErrorMessage;

             
    procedure cb_connect (arg1 : access mosquitto_h.mosquitto;
                          obj : System.Address;
                          reason_code : int)
                          is
        status : Int;
    begin
        Put("On_Connect : ");
        Put_Line(Value(mosquitto_h.mosquitto_connack_string(reason_code)));
        if reason_code /= 0
        then
            status := mosquitto_h.mosquitto_disconnect(arg1);
        end if ;
    end cb_connect ;


    procedure cb_disconnect (arg1 : access mosquitto_h.mosquitto;
                             obj : System.Address;
                             reason_code : int)                          
                          is
    begin
        Put("On_DisConnect ");
        if reason_code /= 0
        then
            Put(Value(mosquitto_h.mosquitto_reason_string(reason_code)));
        end if ;
        New_Line;
    end cb_disconnect ;

   function Create (id : access String := null ) return handle is
        hid : Interfaces.C.Strings.chars_ptr
            := Interfaces.C.Strings.null_ptr ;
      result : handle ;
   begin
        if id /= null
        then
            hid := New_String(id.all);
        end if ;
        result := Handle(mosquitto_h.mosquitto_new( hid ,
                                            true ,
                                            System.Null_Address));
        if result = null
        then
            raise HANDLE_CREATION_FAILURE ;
        end if ;
        if verbose
        then
            mosquitto_h.mosquitto_connect_callback_set( result , cb_connect'access) ;
            mosquitto_h.mosquitto_disconnect_callback_set( result , cb_disconnect'access) ;
        end if;
        return result ;
   end Create ;

   procedure Destroy (h : handle) is
   begin
        mosquitto_h.mosquitto_destroy(h);
   end Destroy ;


begin
   declare
      status : Int;
   begin
      status := mosquitto_h.mosquitto_lib_init;
      if status /= 0
      then 
         raise INITIALIZATION_FAILURE ;
      end if;
   end ;
end mqtt ;
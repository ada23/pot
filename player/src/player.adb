with Ada.Calendar; use Ada.Calendar;
with Ada.Text_Io; use Ada.Text_Io;
with Ada.Integer_Text_Io; use Ada.Integer_Text_Io;
with Interfaces.C; use Interfaces.C;
with GNAT.Calendar.Time_Io ;

with Universally_Unique_Identifiers;
with Universally_Unique_Identifiers.Edit;

with mqtt;
with mqtt.publisher;

procedure Player is
   package UUID_Pkg Renames Universally_Unique_Identifiers;
   package UUID_Edit_Pkg Renames Universally_Unique_Identifiers.Edit;

   procedure player_publish(id : String) is
      h : mqtt.Handle ;
      status : int ;
      major, minor, rev : aliased int ;
      will_topic : aliased String := "potm/time/Exited" ; -- /" & id  ;
      will_msg : aliased String := id ;
   begin
      h := mqtt.Create ;
      mqtt.Publisher.Connect(h, will_topic => will_topic'Access , 
                                will_msg => will_msg'Access );
      --mqtt.Publisher.Connect(h);      
      for n in 1..10
      loop
         Put("Publishing to potm/time/" & id ); Put(n); 
         mqtt.publisher.Publish(h,"potm/time" & "/" & id , 
                                gnat.Calendar.Time_io.Image(Clock,"%H:%M:%S"), qos => 1, 
                                retain => False);
         Put_Line(" Done");
         delay 10.0;
      end loop ;
   end player_publish; 
   id : string := UUID_Edit_Pkg.Image(UUID_Pkg.Create);
begin
   Put_Line(id);
   player_publish(id);
end Player;
